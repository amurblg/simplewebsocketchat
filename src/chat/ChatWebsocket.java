package chat;

import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.api.Session;

//Используется не наследование, а аннотации
@SuppressWarnings("UnusedDeclaration")
@WebSocket
public class ChatWebsocket {
    private ChatService chatService;
    private Session session;

    //При обращении к странице (см. java-скрипты в исходнике index.html) происходит создание сокета:
    public ChatWebsocket(ChatService chatService) {
        this.chatService = chatService;
    }

    //После создания - установка соединения:
    @OnWebSocketConnect
    public void onOpen(Session session) {
        chatService.add(this);
        this.session = session;
    }

    @OnWebSocketMessage
    public void onMessage(String message) {
        chatService.sendMessage(message);
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        chatService.remove(this);
    }

    public void sendString(String message) {
        try {
            session.getRemote().sendString(message);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
