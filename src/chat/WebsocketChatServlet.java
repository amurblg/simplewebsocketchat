package chat;

import org.eclipse.jetty.websocket.servlet.*;

import javax.servlet.annotation.WebServlet;

@WebServlet(name="WebsocketChatServlet", urlPatterns = {"/chat"})
public class WebsocketChatServlet extends WebSocketServlet {
    private final static int LOGOUT_TIME = 10 * 60 * 1000;
    private final ChatService chatService;

    public WebsocketChatServlet() {
        chatService = new ChatService();
    }

    public void configure(WebSocketServletFactory factory) {
        //Конфигурирование сервлета - получаем на вход фабрику по генерации сокетов
        factory.getPolicy().setIdleTimeout(LOGOUT_TIME);//Задаем время выхода по неактивности

        //Что именно делать при установке соединения со стороны пользователя
        factory.setCreator((req, resp) -> new ChatWebsocket(chatService));//Создаем новый сокет
    }
}
