package chat;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ChatService {
    private Set<ChatWebsocket> websockets;//Хранит всех, кто подсоединен к серверу

    public ChatService() {
        websockets = Collections.newSetFromMap(new ConcurrentHashMap<>());
        //ConcurrentHashMap используется вместо обычного HashMap по причине потокобезопасности
    }

    //Рассылка сообщения всем участникам чата
    public void sendMessage(String message) {
        for (ChatWebsocket cws :
                websockets) {
            try {
                cws.sendString(message);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public void add(ChatWebsocket cws) {
        websockets.add(cws);
    }

    public void remove(ChatWebsocket cws) {
        websockets.remove(cws);
    }
}
